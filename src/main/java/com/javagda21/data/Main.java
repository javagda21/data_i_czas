package com.javagda21.data;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
//          NIE UŻYWAĆ!!!!!!!!!!!!!!!!!!!!
//        Date date = new Date();
//        System.out.println(date);

        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        LocalDate datePlusSomeTime = date.plusDays(128736);

//        String dataString1 = "2019-01-25";
        String dataString1 = "01-25-2019";
        String dataString2 = "25/01/2019";

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        LocalDate sparsowanaData = LocalDate.parse(dataString1, formatter);// DateTimeParseException
        System.out.println(sparsowanaData);


        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy <3 MM <3 dd");
        String dataSparsowanaDoTekstu = dateTimeFormatter.format(sparsowanaData);
        System.out.println(dataSparsowanaDoTekstu);


        LocalDate dataDzisiaj = LocalDate.now();
        LocalDate bitwaPodGrunwaldem = LocalDate.of(1410, 7, 15);
        Period period = Period.between(bitwaPodGrunwaldem, dataDzisiaj);
        System.out.println("Dni:" + period.getDays());
        System.out.println("Miesięcy:" + period.getMonths());
        System.out.println("Lat:" + period.getYears());
//        System.out.println("Minut:" + period.get());


        LocalTime czasTeraz = LocalTime.now();
        LocalTime czasPoczatekZajec = LocalTime.of(10, 00, 00, 00);
        Duration duration = Duration.between(czasPoczatekZajec, czasTeraz);
        System.out.println("Sekund: " + duration.getSeconds());
        System.out.println("Minut: " + duration.getSeconds()/60);

    }
}
